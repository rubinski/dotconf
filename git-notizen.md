#GIT Notizen

Liste Status der Dateien

	git status 

Liste alle geänderten Dateien

	git diff

Markiere alle Dateien in der lokalen Struktur als "hochladbar"

	git add --all

Markiere eine bestimmte Datei in der lokalen Struktur als "hochladbar"

	git add filename.txt

Committen

	git commit -m "einzeilige beschreibung der änderungen"

Committen und adden (Kurzform von "add" und "commit")

	git commit -am "einzeilige beschreibung der änderungen"

Lade alle Änderungen zum remote repository hoch

	git push 
	
Lade alle geänderten Dateien vom remote repository herunter

	git pull
	
Workflow generell:

	1. Dateien ändern mit bel. Editor
	2. Dateien adden (einmalig)
	3. Committen (inkl. commit message)
	4. Pushen 